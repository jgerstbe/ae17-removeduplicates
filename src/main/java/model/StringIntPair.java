package model;

/**
 * Created by Jan-Erik on 19.11.2017.
 */
public class StringIntPair {
    String string;
    int integer, hash;

    public StringIntPair(String string, int hash, int integer) {
        this.string = string;
        this.hash = hash;
        this.integer = integer;
    }

    public String getString() {
        return string;
    }

    public void setString(String string) {
        this.string = string;
    }

    public int getInteger() {
        return integer;
    }

    public void setInteger(int integer) {
        this.integer = integer;
    }

    public int getHash() {
        return hash;
    }

    public void setHash(int hash) {
        this.hash = hash;
    }

    @Override
    public String toString() {
        return string + '=' + integer;
    }
}
