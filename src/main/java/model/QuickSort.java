package model;

/**
 * Created by Jan-Erik on 19.11.2017.
 */
import javafx.util.Pair;

import java.util.ArrayList;

public class QuickSort {

    public static ArrayList<StringIntPair> quickSort(ArrayList<StringIntPair> strArr, int p, int r)
    {
        if(p<r)
        {
            int q=partition(strArr,p,r);
            quickSort(strArr,p,q);
            quickSort(strArr,q+1,r);
        }

        return strArr;
    }

    private static int partition(ArrayList<StringIntPair> strArr, int p, int r) {

        String x = strArr.get(p).getString();
        int i = p-1 ;
        int j = r+1 ;

        while (true)
        {
            i++;
            while ( i< r && strArr.get(i).getString().compareTo(x) < 0)
                i++;
            j--;
            while (j>p && strArr.get(j).getString().compareTo(x) > 0)
                j--;

            if (i < j)
                swap(strArr, i, j);
            else
                return j;
        }
    }

    private static void swap(ArrayList<StringIntPair> strArr, int i, int j)
    {
        StringIntPair temp = strArr.get(i);
        strArr.set(i,strArr.get(j));
        strArr.set(j,temp);
    }
}