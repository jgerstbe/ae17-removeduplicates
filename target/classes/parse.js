var fs = require('fs');
var data = [];
for (let i = 1; i< 11; i++) {
	console.log("reading file resource"+i+".json");
	var d = JSON.parse(fs.readFileSync('response'+i+'.json', 'utf8'));
	data = data.concat(d.resource);
}
console.log(data.length+" users");
var logger = fs.createWriteStream('data.txt', {
	flags: 'a'
});
for (let i = 0; i < data.length; i++) {
	console.log(i+": "+data[i].email+" "+data[i].userID+" "+data[i].userLastLogin);
	if (data[i].email) {
		if (data[i].userLastLogin == "0000-00-00 00:00:00") {
			data[i].userLastLogin  = 0;
		}
		logger.write(data[i].email+" "+data[i].userID+" "+Math.floor(new Date(data[i].userLastLogin) / 1000)+"\n");
	}
}
logger.end();