package model;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.io.PrintWriter;
import java.io.File;
import java.io.FileNotFoundException;

public class FileIO {
    public static ArrayList<StringIntPair> loadAsPair(String fileName, Integer size) {
        ArrayList<StringIntPair> map = new ArrayList<StringIntPair>();

        try {
            FileReader fr = new FileReader(fileName);
            BufferedReader br = new BufferedReader(fr);

            String line = br.readLine();
            String[] strings = new String[2];

            int i = 0;
            while ((line !=null) && (i <= size)) {
                if (i > 0) {
                    strings = line.split(" ");
                    map.add(new StringIntPair(strings[0], strings[0].hashCode(), Integer.parseInt(strings[1])));
                }
                line = br.readLine();
                i++;
            }


        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return map;
    }

    public static void writeToFile(double[] durationArrayList) {

        PrintWriter printWriter = null;

        try {
            printWriter = new PrintWriter(new File("src/main/resources/output.txt"));
            for (double a : durationArrayList) {
                printWriter.println(a);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        System.out.println("Ergebnisse wurden in output.txt gepseichert");
        printWriter.close();
    }

    public static void writeToFile(String file, double[][] durationArrayList) {
        try {
            PrintWriter printWriter = new PrintWriter(new File("src/main/resources/output-"+file+".txt"));
            printWriter.println("n\tEX\tAnzahl\tsdv\tEXsdv\tAnzahl\tkleiner\tgrößer\tEX10%\tAnzahl\tEXboxplot\tQ025\tQ050\tQ075\ta\tb\tmin\tmax");
            for (int j = 0; j < durationArrayList.length; j++) {
                String line = "";
                for (int i = 0;i < durationArrayList[j].length;i++) {
                    line += durationArrayList[j][i]+"\t";
                }
                printWriter.println(line);
            }
            printWriter.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static void boxToFile(String file, double[][] list) {
        try {
            PrintWriter printWriter = new PrintWriter(new File("src/main/resources/output-"+file+".txt"));
            printWriter.println("% N erw stdv q25 q75 median uwhi owhi boxwh");
            Integer i = 1;
            for (double[]l : list) {
                printWriter.print(i+" "+l[0]+" "+l[10]+" "+l[3]+" "+l[11]+" "+l[13]+" "+l[12]+" "+l[14]+" "+l[15]+" 0.2");
                printWriter.print("\n");
                i++;
            }
            printWriter.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

}