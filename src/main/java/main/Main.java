package main;

import model.FileIO;
import model.QuickSort;
import model.StringIntPair;
import model.Statistics;

import java.util.ArrayList;

public class Main {
    public static void main(final String[] args) {
        //total order --> a > b, b > c, ... oder anders herum -> string alphabetisch sortiert
        //partial order --> auxillary data für sich sortieren pro key


        //String name = "real-";
        //Integer[] problems = {1000, 2500, 5000, 7500, 10000};
        //String name = "testData";
        //Integer[] problems = {1000, 2500, 5000, 7500, 10000, 20000, 50000, 100000, 150000, 200000};

        for (int t = 1; t<3; t++) {
            //String name = "testLZ-realData-A"+t+"-";
            //String file = "src/main/resources/data.txt";
            //Integer[] problems = {1000, 2000, 4000, 8000, 10000};
            String name = "testLZ-A"+t+"-";
            String file = "src/main/resources/500k.txt";
            Integer[] problems = {1000, 2000, 4000, 5000, 8000, 10000, 16000, 20000, 30000, 32000, 40000, 50000, 64000, 100000};
            double[][][] comparison = new double [4][problems.length][18];
            double[][][] messungen = new double [problems.length][4][18];
            for (int i = 0; i < problems.length; i++) {
                double[][] test;
                if (t == 1) {
                    test = runTestA1(file, problems[i], 40);
                } else {
                    test = runTestA2(file, problems[i], 40);
                }
                messungen[i][0] = Statistics.evaluateDurations(problems[i] , test[0]);
                messungen[i][1] = Statistics.evaluateDurations(problems[i] , test[1]);
                messungen[i][2] = Statistics.evaluateDurations(problems[i] , test[2]);
                messungen[i][3] = Statistics.evaluateDurations(problems[i] , test[3]);
                FileIO.writeToFile(name+"stats-"+problems[i].toString(), messungen[i]);
                comparison[0][i] = messungen[i][0];         //vgl alle gesamt
                comparison[1][i] = messungen[i][1];         //vgl alle hash
                comparison[2][i] = messungen[i][2];         //vgl alle compareOnDemand
                comparison[3][i] = messungen[i][3];         //vgl alle qsOnDemand
            }
            FileIO.writeToFile(name+"vgl-gesamt", comparison[0]);
            FileIO.boxToFile(name+"box-gesamt", comparison[0]);
            FileIO.writeToFile(name+"vgl-hash", comparison[1]);
            FileIO.boxToFile(name+"box-hash", comparison[1]);
            FileIO.writeToFile(name+"vgl-compareOnDemand", comparison[2]);
            FileIO.boxToFile(name+"box-compareOnDemand", comparison[2]);
            FileIO.writeToFile(name+"vgl-qsOnDemand", comparison[3]);
            FileIO.boxToFile(name+"box-qsOnDemand", comparison[3]);
        }
    }

    /*
        nach index sortieren und dann verlgeichen (--> kann mann dann in hashmap packen, diese behält nur den 'größten' key)
        vs
        vergleichen und ggf. aktives und gefundenenes "sortieren" --> behaltenen abspeichern und am ende die liste mit allen droppen
     */

    public static ArrayList<StringIntPair> loadData(String source, Integer size) {
        if (size > 500000) {
            System.out.println("Maximale Länge ist 500 000!");
            size = 500000;
        }
        ArrayList<StringIntPair> testData = FileIO.loadAsPair(source, size);
        System.out.println("Anzahl Elemente in testData: " + testData.size());
        return testData;
    }

    public static ArrayList<StringIntPair> compareOnDemand(ArrayList<StringIntPair> list, boolean presorted) {
        ArrayList<StringIntPair> result = new ArrayList<StringIntPair>();
        while (!list.isEmpty()) {
            StringIntPair pair = list.get(0);
            list.remove(0);
            for (int j = 0; j < list.size(); j++) {
                StringIntPair otherPair = list.get(j);
                int p = pair.getHash();
                int op = otherPair.getHash();
                if (p == op) {
                    list.remove(otherPair);
                    if (pair.getInteger() < otherPair.getInteger()) {
                        pair = otherPair;
                    }
                }
                if (presorted && (p != op)) break; //break wenn hash der pairs unterschiedlich ist und es der 2e algo ist
            }
            result.add(pair);
        }
        return result;
    }


    public static double[][] runTestA1(String source, Integer size, Integer i) {
        double[][] times = new double[4][i];
        //optimizing
        for (int c = 0; c <10; c++) {
            ArrayList<StringIntPair> l = compareOnDemand(loadData(source, size), false);
            QuickSort.quickSort(l, 0, l.size()-1);
        }
        //actual test
        for (int c = 0; c < i; c++) {
            double start1 = System.nanoTime();
            double start2 = System.nanoTime();
            ArrayList<StringIntPair> list = loadData(source, size);
            double stopLoad1 = System.nanoTime();
            double stopLoad2 = System.nanoTime();
            ArrayList<StringIntPair> results = compareOnDemand(list, false);
            double stopCompare1 = System.nanoTime();
            double stopCompare2 = System.nanoTime();
            QuickSort.quickSort(results, 0, results.size()-1);
            double stop = System.nanoTime();

            double duration = stop - 2*start2 + start1;
            double durationLoad = stopLoad1 - 2*start2 + start1;
            double durationCompare = stopCompare1 - 2*stopLoad2 + stopLoad1;
            double durationQs = stop - 2*stopCompare2 + stopCompare1;
            times[0][c] = duration/1000000F;
            times[1][c] = durationLoad/1000000F;
            times[2][c] = durationCompare/1000000F;
            times[3][c] = durationQs/1000000F;
            System.out.println("Run "+c+" durationLoad "+durationLoad+"ns == "+(durationLoad/1000000F)+"ms == "+(durationLoad/1000000000F)+"s");
            System.out.println("Run "+c+" durationCompare "+durationCompare+"ns == "+(durationCompare/1000000F)+"ms == "+(durationCompare/1000000000F)+"s");
            System.out.println("Run "+c+" durationQs "+durationQs+"ns == "+(durationQs/1000000F)+"ms == "+(durationQs/1000000000F)+"s");
            System.out.println("Run "+c+" took "+duration+"ns == "+(duration/1000000F)+"ms == "+(duration/1000000000F)+"s");
        }
        return times;
    }

    public static double[][] runTestA2(String source, Integer size, Integer i) {
        double[][] times = new double[4][i];
        //optimizing
        for (int c = 0; c <10; c++) {
            ArrayList<StringIntPair> l = loadData(source, size);
            l = QuickSort.quickSort(l, 0, l.size()-1);
            l = compareOnDemand(l, true);
        }
        //actual test
        for (int c = 0; c < i; c++) {
            double start1 = System.nanoTime();
            double start2 = System.nanoTime();
            ArrayList<StringIntPair> list = loadData(source, size);
            double stopLoad1 = System.nanoTime();
            double stopLoad2 = System.nanoTime();
            ArrayList<StringIntPair> sorted = QuickSort.quickSort(list, 0, list.size()-1);;
            double stopCompare1 = System.nanoTime();
            double stopCompare2 = System.nanoTime();
            sorted = compareOnDemand(sorted, true);
            double stop = System.nanoTime();

            double duration = stop - 2*start2 + start1;
            double durationLoad = stopLoad1 - 2*start2 + start1;
            double durationQs = stopCompare1 - 2*stopLoad2 + stopLoad1;
            double durationCompare = stop - 2*stopCompare2 + stopCompare1;
            times[0][c] = duration/1000000F;
            times[1][c] = durationLoad/1000000F;
            times[2][c] = durationCompare/1000000F;
            times[3][c] = durationQs/1000000F;
            System.out.println("Run "+c+" durationLoad "+durationLoad+"ns == "+(durationLoad/1000000F)+"ms == "+(durationLoad/1000000000F)+"s");
            System.out.println("Run "+c+" durationCompare "+durationCompare+"ns == "+(durationCompare/1000000F)+"ms == "+(durationCompare/1000000000F)+"s");
            System.out.println("Run "+c+" durationQs "+durationQs+"ns == "+(durationQs/1000000F)+"ms == "+(durationQs/1000000000F)+"s");
            System.out.println("Run "+c+" took "+duration+"ns == "+(duration/1000000F)+"ms == "+(duration/1000000000F)+"s");
        }
        return times;
    }

    public long mittelwert(ArrayList<Long> list) {
        long summe = 0;
        for (Long element:list) {
            summe += element;
        }
        return summe / list.size();
    }
}